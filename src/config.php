<?php
/* define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_NAME', 'login-repeat');

//connect to databse
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

//check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());

} */

//These are the defined authentication environment in the db service

// The MySQL service named in the docker-compose.yml.
$host = 'db';

// Database use name
$user = 'root';

//database user password
$pass = 'MYSQL_ROOT_PASSWORD';

// database name
$mydatabase = 'MY_DATABASE';
// check the mysql connection status

$conn = new mysqli($host, $user, $pass, $mydatabase);

// Check connection
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: " . $conn->connect_error;
    exit();
}

//creates a table only or the first time
if ($result = $conn->query("

create table if not exists `login` (
    id int not null auto_increment,
    username text not null,
    password text not null,
    primary key (id)
);
")) {
    echo "table was created";
};
