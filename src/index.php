<?php
//init session
session_start();

//check if user is already logged in,if yes, redirect to welcome page

if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    header("location: welcome.php");
    exit;
}

//include config
require_once "config.php";
//define varialbles nd inizializate


//test
$username = '';
$password = '';
$username_err = '';
$password_err = '';
$login_err = '';

//processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //Check if username is empty
    if (empty(trim($_POST['username']))) {
        $username_err = 'please enter username';
    } else {
        $username = trim($_POST['username']);
    }
    //check if passwordis empty
    if (empty(trim($_POST['password']))) {
        $password_err = "please enter password";
    } else {
        $password = trim($_POST['password']);
    }

    //validate credentials
    if (empty($username_err) && empty($password_err)) {
        //prepare select statement
        $sql = "SELECT id, username, password FROM login WHERE username = ?";
        if ($stmt = mysqli_prepare($conn, $sql)) {
            //bind variables to prep stmt as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            //set parameters
            $param_username = $username;

            //attemp to execute the prepared stmt
            if (mysqli_stmt_execute($stmt)) {
                //store result
                mysqli_stmt_store_result($stmt);

                //chekc if username exists, if yes then verify password
                if (mysqli_stmt_num_rows($stmt) == 1) {
                    //bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if (mysqli_stmt_fetch($stmt)) {
                        if (password_verify($password, $hashed_password)) {
                            //password is correct, start a new session gives error!
                            /* session_start(); */

                            //store data i session variables

                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;

                            //redirect user to welcome page
                            header("location: welcome.php");
                        } else {
                            //password is not valid, display generic error msg
                            $login_err = "invalid username or password";
                        }
                    }
                } else {
                    //username does not exist
                    $login_err = "invalid username or psw";
                }
            } else {
                echo "oops, something smells bad";
            }
            //close statement
            mysqli_stmt_close($stmt);
        }
    }
    //close connection
    mysqli_close($conn);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body {
            font: 14px sans-serif;
        }

        .wrapper {
            width: 360px;
            padding: 20px;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <h2>Login</h2>
        <p>Please fill in your credentials to login.</p>

        <?php
        if (!empty($login_err)) {
            echo '<div class="alert alert-danger">' . $login_err . '</div>';
        }
        ?>

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $username; ?>">
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Login">
            </div>
            <p>Don't have an account? <a href="register.php">Sign up now</a>.</p>
        </form>
    </div>
</body>

</html>